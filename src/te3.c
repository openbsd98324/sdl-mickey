
#include <stdlib.h>
#include <stdio.h>
#include "SDL/SDL.h" 
#include "SDL/SDL_image.h" 
#include "SDL/SDL_ttf.h" 

// do you need opengl for that?
#include "SDL/SDL_opengl.h"   

 
/* 
 Under Linux:
 1)apt-get install libsdl-ttf2.0-dev   libsdl-image1.2-dev  libsdl-ttf2.0-dev  libghc6-sdl-ttf-dev 
 2)arial.ttf shall be into the directory: 
    http://www.font-police.com/classique/sans-serif/arial.ttf 

 // to compile: 
    gcc -lncurses -lSDL te.c  -lSDL_ttf
    then:
    ./a.out
*/




SDL_Surface* screen, *logo;
SDL_Rect logorect;

SDL_Surface* fontSurface;
SDL_Color fColor;
SDL_Rect fontRect;

SDL_Event Event;

TTF_Font* font;

//Initialize the font, set to white
void fontInit(){
        TTF_Init();
        font = TTF_OpenFont("arial.ttf", 12);
        fColor.r = 255;
        fColor.g = 255;
        fColor.b = 255;
}



int sprite_x = 4; 
int sprite_y = 4; 

void sdlrectangle(int y1, int x1, int y2, int x2, int color) {
	SDL_Rect rect = {x1 ,y1 ,  (x2-x1),  (y2-y1) };
	SDL_FillRect(screen, &rect, color);
}

void sdlrectanglered(int y1, int x1, int y2, int x2, int color) {
	SDL_Rect rect = {x1 ,y1 ,  x2-x1,  y2-y1 };
	SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 240, 10, 0));
        //SDL_Flip(screen);
}

void sdlprint(int y, int x, char *c){
        fontSurface = TTF_RenderText_Solid(font, c, fColor);
        fontRect.x = x;
        fontRect.y = y;
        SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));
        SDL_BlitSurface(fontSurface, NULL, screen, &fontRect);
}



SDL_Color sdlcolor[10];

int main(int argc, char** argv)
{

logorect.x = 100 ;
logorect.y = 120 ;

    // Initialize the SDL library with the Video subsystem
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE);

    //Create the screen
    screen = SDL_SetVideoMode(640, 480, 0, SDL_SWSURFACE | SDL_FULLSCREEN);

    //Initialize fonts
    fontInit();

    //Print to center of screen
    //printF("Hello World", screen->w/2 - 11*3, screen->h/2);
    // printF("Hello World", screen->w/2 - 11*3, screen->h/2);


    char textit[1000];
    strncpy( textit, "hello world" , 1000 );

    sdlprint( 1 , 1,  textit );
    SDL_Flip(screen);

    SDL_Rect rect = {0 ,0 ,  50, 50 };
    int sdlcolor = 0;
    SDL_Rect pos ;

    int i;

   /*
    sdlcolor[0] = SDL_MapRGB(screen->format,255,255,255);
    sdlcolor[1] = SDL_MapRGB(screen->format,255,0,0); 
   */


    do {

        sdlrectanglered( sprite_y, sprite_x, sprite_y+3, sprite_x+3, 0);
        SDL_Flip(screen);

        // Process the events
        while (SDL_PollEvent(&Event)) {
            switch (Event.type) {

                case SDL_KEYDOWN:

                    switch (Event.key.keysym.sym) {

                        case SDLK_DELETE:
                          strncpy( textit, "" , 1000 );
                          break;

                        case SDLK_LEFT:
                          sprite_x--;
                          break;
                        case SDLK_RIGHT:
                          sprite_x++;
                          break;
                        case SDLK_UP:
                          sprite_y--;
                          break;
                        case SDLK_DOWN:
                          sprite_y++;
                          break;


                        case SDLK_u:
                          sdlcolor++;
                          break;

                        case SDLK_d:
                          sdlcolor--;
                          break;


                        case SDLK_t:
  			  sdlrectangle( 0, 0, 30, 200, SDL_MapRGB(screen->format,155,155,155));  
	                  SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format,155,155,155));  
                          sdlprint( 300, 5 , textit);
                          break;

                        case SDLK_a:
                           logo = SDL_LoadBMP("./a.bmp");
                           SDL_BlitSurface( logo , NULL , screen , &logorect );
                          break;
                        case SDLK_b:
                           logo = SDL_LoadBMP("./b.bmp");
                           SDL_BlitSurface( logo , NULL , screen , &logorect );
                          break;
                        case SDLK_c:
                           logo = SDL_LoadBMP("./c.bmp");
                           SDL_BlitSurface( logo , NULL , screen , &logorect );
                          break;

                        case SDLK_p:
                        case SDLK_l:
                           logo = SDL_LoadBMP("./mi.bmp");
                           SDL_BlitSurface( logo , NULL , screen , &logorect );
                          break;

                        case SDLK_o:
  			  sdlrectanglered( 0, 0, 60, 200, 0);
                          break;


                        case SDLK_r:
  			  sdlrectangle( 0, 0, 30, 200, SDL_MapRGB(screen->format,0,55,95));  
                          sdlprint( 5, 5 , textit);
                          break;

                        case SDLK_RETURN:
                          strncpy( textit, "You pressed Return" , 1000 );
                          sdlprint( 5, 5 , textit);
                          break;

                    // Escape forces us to quit the app
                        case SDLK_ESCAPE:
                            Event.type = SDL_QUIT;
                        break;

                        default:
                        break;
                    }
                break;

            default:
            break;
        }
    }
    SDL_Delay(10);
    } while (Event.type != SDL_QUIT);

    // Cleanup
    SDL_Quit();

    return 0;
}


